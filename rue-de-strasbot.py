'''
Discord Bot for a server for coordinating boardgames.
By Zach Smith

This bot is designed to facilitiate parts of a Discord server
around a boardgames group. Users interact with the bot by dropping
emotes on messages created by the bot (instead of memorizing commands).

The bot is written with a physical device in mind - a Raspberry pi connected
to speakers and a webcam.

Main functionalities:
    - Making a doorbell sound (since the one in my apartment doesn't have my name attached to it)
    - Taking photos (for happy memories)

The bot is intended to interact with the following channels:
    - A "doorbell" channel, where an emote to ring the doorbell can be dropped
    - A "take photo" channel, where an emote to take a photo can be dropped
    - A "view photos" channel, where the produced photos are stored and can be deleted

Note that the following files must be provided:
    - .env, which contains the keys DISCORD_TOKEN and DISCORD_GUILD for the discord API token and server name (as text)
    - ./doorbell-sounds/doorbell.mp3 (see DOORBELL_SOUND) as the sound that plays when the doorbell rings

Marks (ctrl+f to find):
    - Constants
    - Doorbell
    - Photos
    - Setup
    - Initialization

'''

import asyncio
import time
import sys
import os
import requests

import numpy as np
import time

import discord
from discord.ext import commands

from dotenv import load_dotenv

import yaml

from datetime import datetime, timedelta
from playsound import playsound

# Pip3 libraries:
# discord.py asyncio pyyaml numpy python-dotenv datetime playsound requests

client = discord.Client()
intents = discord.Intents.default()
intents.reactions = True
intents.members = True

#-----------------------------------------------------------------------------#
#---------------------------------- Constants --------------------------------#
#-----------------------------------------------------------------------------#

# Looks like a space, but counts as a character
# So discord will not squelch it as if it were whitespace
ZERO_WIDTH_SPACE = '\u200B'
SEC_IN_DAY = 86400

# -- Channel info
DOORBELL_CHANNEL = "doorbell"
TAKE_PHOTOS_CHANNEL = "camera"
SEND_PHOTOS_CHANNEL = "food-photos"

# -- Emote names
RING_DOORBELL_EMOTE = "bell"
TAKE_PHOTO_EMOTE = "camera_with_flash"
ACCEPT_PHOTO_EMOTE = "white_check_mark"
DELETE_PHOTO_EMOTE = "x"
RING_DOORBELL_EMOTE_UNICODE = "🔔"
TAKE_PHOTO_EMOTE_UNICODE = "📸"
ACCEPT_PHOTO_EMOTE_UNICODE = "✔️"
DELETE_PHOTO_EMOTE_UNICODE = "❌"

# --
DOORBELL_SOUND = "doorbell-sounds/doorbell.mp3"
DOORBELL_CD_IN_MINUTES = 2

#-----------------------------------------------------------------------------#
#----------------------------------- Doorbell --------------------------------#
#-----------------------------------------------------------------------------#
async def on_doorbell(context):
    # Remove/reset the reaction
    await client.doorbellMessage.clear_reactions()
    await client.doorbellMessage.add_reaction("🔔")

    # Set the doorbell to be on cooldown
    delta = (datetime.now() - client.lastDoorbell)/timedelta(minutes=1)
    if delta < 3:
        print(f"  -- on_doorbell | Doorbell rang too recently (lastring: {client.lastDoorbell}, time now: {datetime.now()}) --")
        return False
    
    # Do the doorbell
    client.lastDoorbell = datetime.now()
    print("  -- on_doorbell | Dingdong! --")

    # Trigger switchbot
    requests.get(url=f"https://maker.ifttt.com/trigger/{SWITCHBOT_EVENT}/with/key/{IFTTT_KEY}")

    # TODO make sure this is called in another thread, spamming the emote can block the sound
    #await play_doorbell_sound()

async def play_doorbell_sound():
    playsound(DOORBELL_SOUND) # Second argument is play async


#-----------------------------------------------------------------------------#
#------------------------------------ Camera ---------------------------------#
#-----------------------------------------------------------------------------#


#-----------------------------------------------------------------------------#
#---------------------------------- Utility ----------------------------------#
#-----------------------------------------------------------------------------#

async def nukeChannel(channel):
    messages = await channel.history().flatten()
    for message in messages:
        await message.delete()

#-----------------------------------------------------------------------------#
#-------------------------- Called/Scheduled Tasks ---------------------------#
#-----------------------------------------------------------------------------#

# Scheduled task that runs every 60 seconds.
# Doesn't actually do anything
async def schedule_periodic_task():
    await client.wait_until_ready()
    await asyncio.sleep(5) # Dirty hack to make sure on_ready has actually finished running
    await periodic_task() # Initialise any variables stored in the periodic task

    # Find out how long until the next minute (e.g.) starts
    timeNow = datetime.now()
    delta = timedelta(minutes=1)
    nextMinute = (timeNow + delta).replace(second=0)
    timeToWait = (nextMinute-timeNow).seconds

    await asyncio.sleep(timeToWait)
    while True:
        await periodic_task()

        # Calculate next wait time
        timeNow = datetime.now()
        delta = timedelta(minutes=1)
        nextMinute = (timeNow + delta).replace(second=0)
        timeToWait = (nextMinute-timeNow).seconds

        # Actually wait
        await asyncio.sleep(timeToWait)

async def periodic_task():
    #await client.debugChannel.send("I am a gnome and I live in a dome")
    pass


#-----------------------------------------------------------------------------#
#---------------------------------- Setup ------------------------------------#
#-----------------------------------------------------------------------------#

# Called after the bot first connects to the server
@client.event
async def on_ready():
    for g in client.guilds:
        if g.name == GUILD:
            client.guild = g

    print(f"---- Connected to Server {client.guild.name} ----")

    # Initialize channels as variables
    for channel in client.guild.channels:
        if channel.name == DOORBELL_CHANNEL:
            client.doorbellChannel = channel
        if channel.name == TAKE_PHOTOS_CHANNEL:
            client.takePhotosChannel = channel
        if channel.name == SEND_PHOTOS_CHANNEL:
            client.sendPhotosChannel = channel

    await initialize_channels()

async def initialize_channels():
    await nukeChannel(client.doorbellChannel)
    #await nukeChannel(client.takePhotosChannel)

    # Doorbell channel message
    client.doorbellMessage = await client.doorbellChannel.send(f":{RING_DOORBELL_EMOTE}:")
    client.lastDoorbell = datetime.now() - timedelta(minutes=DOORBELL_CD_IN_MINUTES)
    await client.doorbellMessage.add_reaction("🔔")

    # Photo channel message
    pass

@client.event
async def on_raw_reaction_add(context):
    message_id = context.message_id # int
    emoji = context.emoji # PartialEmoji
    member = context.member # Member?

    if member is None:
        print("  -- on_raw_reaction_add | No member --")
        return False

    if member.id == client.user.id:
        print("  -- on_raw_reaction_add | Member is discordClient --")
        return False

    if (message_id == client.doorbellMessage.id):
        print("  -- on_raw_reaction_add | Dingdong! --")
        await on_doorbell(context)

#-----------------------------------------------------------------------------#
#---------------------------- Initialization ---------------------------------#
#-----------------------------------------------------------------------------#

# Get hidden environment variables (safety first, kids!)
print("---- Rue de StrasBot Initialise ----")
load_dotenv()
TOKEN = os.getenv('DISCORD_TOKEN')
GUILD = os.getenv('DISCORD_GUILD')
SWITCHBOT_EVENT = os.getenv('SWITCHBOT_EVENT')
IFTTT_KEY = os.getenv('IFTTT_KEY')

if TOKEN is None:
    sys.exit("---- Tokens failed to load, do you have the .env file? ----")

# This adds an asynchronous task to spam the messages of the day
# I still don't actually understand how it works, but okay.
client.loop.create_task(schedule_periodic_task())

# Actually kick off the bot
print("---- Firing up the Bot ----")
client.run(TOKEN)
